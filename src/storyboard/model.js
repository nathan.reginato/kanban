import { compose, concat, map } from "ramda";
import Task from "data.task";

const Url = String;

const http = {
    // get :: Url -> Task Error JSON
    get : function(Url) {
        return new Task((rej, res) => fetch(Url)
        .then(res => res.json())
        .catch(rej)
        .then(res))}
}
const wrapInArray = (x) => [x];

const storyApiUrl = "http://localhost:8001/story/";

// makeUrl :: Int -> Url
const makeUrl = concat(storyApiUrl)

// getStory :: String -> Story
const getStory = compose(map(wrapInArray), http.get, makeUrl);

// getAllStories -> Task Error [Story]
const getAllStories = http.get(storyApiUrl);

module.exports = { getStory, getAllStories }