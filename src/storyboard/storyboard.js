import React from "react";
import createReactClass from "create-react-class";
import { getStory, getAllStories } from "./model";
import Story from "../story/story";
import { Input, Button } from 'antd';
import "./storyboard.css";

const Search = Input.Search;

const StoryBoard = createReactClass({
  displayName: "StoryBoard",

  // getInitialState :: { storyID :: String, results :: [] }
  getInitialState() { return { storyID: "", results: []} },

  // onStoryIDChanged :: Event -> State storyID
  onStoryIDChanged({target: t}) { this.setState({storyID: t.value}) },

  // updateResults :: Event -> State results
  updateResults(xs) { this.setState({storyID: "", results: xs}) },

  // searchCilicked :: Event -> ?
  searchClicked(_) { getStory(this.state.storyID).fork(this.props.showError, this.updateResults) },

  // getAllStories :: Event -> ?
  getAllStories(_) { getAllStories.fork(this.props.showError, this.updateResults) },

  render() {
    return(
        <div id="storyboard">
             <Search
                className="storyboard"
                placeholder="Story ID"
                onSearch={this.searchClicked}
                onChange={this.onStoryIDChanged}
                style={{ width: 200 }}
                type="number"
                enterButton
            />
            <Button type="primary" onClick={this.getAllStories}>Get Stories</Button>
            <div id="story-conatiner">
                {this.state.results.map(x => <Story key={x.ID} data={x}></Story>)}
            </div>
        </div>
    )}
});

export default StoryBoard;