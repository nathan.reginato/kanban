import React from "react";
import createReactClass from "create-react-class";
import "./story.css"
import { Card } from "antd";

const StoryBoard = createReactClass({
  displayName: "Story",

  // getInitialState :: {  data :: Object }
  getInitialState() { return { data: this.props.data } },

  render() {
    return(
        <Card className="story" title={this.props.data.Title} extra={<a href="#">More</a>}>
            <p>{this.props.data.Description}</p>
            <p>{this.props.data.State}</p>
        </Card>
    )}
});

export default StoryBoard;