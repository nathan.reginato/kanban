import React from "react";
import createReactClass from "create-react-class";
import "./story.css"

const StoryBoard = createReactClass({
  displayName: "Lane",

  // getInitialState :: {  data :: Object }
  getInitialState() { return { stories: this.props.stories } },

  render() {
    return(
        <div className="lane">
           {this.props.stories.map(story => <Story data={story}></Story>)}
        </div>
    )}
});

export default StoryBoard;