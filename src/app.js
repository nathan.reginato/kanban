import React from "react";
import createReactClass from "create-react-class";
import StoryBoard from "./storyboard/storyboard";
import { message } from 'antd'

let App = createReactClass({
  displayName: "App",

  // getInitialState :: { error :: String }
  getInitialState: function() {return {error: ""}},

  // showError :: String -> State Error
  showError: function(e) { 
    this.setState({error: e.message});
    message.error(e.message);
  },

  render() {
    return (
    <div id="App">
      <StoryBoard showError={this.showError} />
    </div>
  )}
});

export default App;
